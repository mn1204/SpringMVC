package com.varlor.service;

import com.varlor.entity.User;

import java.util.List;

public interface LoginService {
    public List<User> login(User user);
}
