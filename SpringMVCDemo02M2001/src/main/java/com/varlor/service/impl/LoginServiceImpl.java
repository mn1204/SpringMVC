package com.varlor.service.impl;

import com.varlor.entity.User;
import com.varlor.mapper.LoginMapper;
import com.varlor.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    public LoginMapper loginMapper;
    @Override
    public List<User> login(User user) {
        return loginMapper.login(user);
    }
}
