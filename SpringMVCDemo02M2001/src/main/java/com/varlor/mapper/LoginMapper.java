package com.varlor.mapper;

import com.varlor.entity.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LoginMapper {

    public List<User> login(User user);
}
