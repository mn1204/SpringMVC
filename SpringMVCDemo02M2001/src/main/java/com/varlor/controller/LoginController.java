package com.varlor.controller;

import com.varlor.entity.User;
import com.varlor.service.LoginService;
import com.varlor.service.impl.LoginServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class LoginController {
    @Autowired
    public LoginService loginService;
    @RequestMapping("/login")
    public String login(User user){
        List<User> login = loginService.login(user);
        if (login.size()>0){
            return "hello";
        }
        return "nohello";
    }

}
